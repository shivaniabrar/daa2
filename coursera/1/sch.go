package main

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strconv"
	"strings"
)

var score []float64

type Jobs struct {
	w     int
	l     int
	score float64
}

type jobSorter struct {
	m    map[float64][]*Jobs
	jobs []*Jobs
	by   func(p1, p2 *Jobs) bool
}

func (s *jobSorter) makemap() *jobSorter {

	s.m = make(map[float64][]*Jobs)
	for i, job := range s.jobs {
		s.m[s.jobs[i].score] = append(s.m[s.jobs[i].score], job)
	}
	return s
}

func (s *jobSorter) sortmap() *jobSorter {
	for _, v := range s.m {
		j := &jobSorter{
			jobs: (v),
			by:   s.by,
		}
		sort.Sort(j)
	}
	return s
}

func (s *jobSorter) Len() int {
	return len(s.jobs)
}

func (s *jobSorter) Swap(i, j int) {
	s.jobs[i], s.jobs[j] = s.jobs[j], s.jobs[i]
}

func (s *jobSorter) Less(i, j int) bool {
	return s.by(s.jobs[i], s.jobs[j])
}

func input() []*Jobs {

	filecontains, _ := ioutil.ReadFile("jobs.txt")
	line := strings.Split(string(filecontains), "\n")
	tmp, _ := strconv.Atoi(line[0])
	jobs := make([]*Jobs, tmp)

	for i, v := range line {
		if i == 0 {
			continue
		}
		j_property := strings.Fields(v)

		if len(j_property) == 0 {
			break
		}

		job := new(Jobs)
		job.w, _ = strconv.Atoi(j_property[0])
		job.l, _ = strconv.Atoi(j_property[1])
		jobs[i-1] = job

	}
	return jobs
}

func calculate_score(jobs []*Jobs, aggregate func(jobs *Jobs) float64) []*Jobs {
	var myscore = make(map[float64]bool)

	for _, val := range jobs {
		val.score = -1 * aggregate(val)
		myscore[val.score] = true
	}

	for key, _ := range myscore {
		score = append(score, key)
	}

	sort.Float64s(score)

	return jobs
}

func weight_wrapper(j *jobSorter) (sum int64) {
	sum = int64(0)
	tot_len := int64(0)
	for _, s := range score {
		sum, tot_len = weight(j.m[s], sum, tot_len)
	}
	return
}

func weight(jobs []*Jobs, sum, tot_len int64) (int64, int64) {

	for _, val := range jobs {

		tot_len += int64(val.l)

		sum += int64(val.w) * tot_len

	}
	return sum, tot_len
}

func main() {

	score := func(p1, p2 *Jobs) bool {
		return p1.score > p2.score
	}

	w := func(p1, p2 *Jobs) bool {
		return p1.w > p2.w
	}

	f1 := func(jobs *Jobs) float64 {
		return float64(jobs.w - jobs.l)
	}

	// f2 := func(jobs *Jobs) float64 {
	// 	return float64(jobs.w) / float64(jobs.l)
	// }

	jobs := calculate_score(input(), f1)

	jb := &jobSorter{
		jobs: jobs,
		by:   score,
	}

	jb.makemap()
	sort.Sort(jb)

	jb.by = w
	jb.sortmap()
	fmt.Println(weight_wrapper(jb))
}
