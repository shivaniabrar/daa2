package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

/* Element */

type element struct {
	dest string
	cost int64
}

func New_element_string(val string) (string, *element) {
	tuple := strings.Fields(val)
	source := tuple[0]
	cost, _ := strconv.ParseInt(tuple[2], 10, 64)
	return source, New_element(tuple[1], cost)
}

func New_element(dest string, cost int64) (ele *element) {
	ele = new(element)
	ele.dest = dest
	ele.cost = cost
	return
}

/* Graph */
type adjList []*element
type graph map[string]adjList

func New_graph() *graph {
	g := make(graph)
	return &g
}

func (g *graph) parse(lines []string) (*graph, string) {
	var (
		source string
		ele    *element
	)

	for _, val := range lines {
		source, ele = New_element_string(val)
		(*g)[source] = append((*g)[source], ele)
		(*g)[ele.dest] = append((*g)[ele.dest], New_element(source, ele.cost))
	}
	return g, source
}

func (g *graph) prims(source string) int64 {

	mst := int64(0)
	dest := ""

	x := make(map[string]bool)
	x[source] = true

	for len(x) < len(*g) {
		min := int64(9999999999999999)
		for key, _ := range x {
			for _, ele := range (*g)[key] {
				b := x[ele.dest]
				if b == false && (ele.cost < min) {
					min = ele.cost
					dest = ele.dest
				}
			}
		}
		mst += min
		x[dest] = true
	}
	return mst
}

/* Helping function */
func readlines(filename string) []string {
	filecontains, _ := ioutil.ReadFile(filename)
	line := strings.Split(string(filecontains), "\n")
	return line[:(cap(line) - 1)]
}

func input() (g *graph, source string) {
	lines := readlines("edges.txt")
	g = New_graph()
	_, source = g.parse(lines[1:])
	return
}

func main() {
	g, source := input()
	mst := g.prims(source)
	fmt.Println("MST :", mst)
}
